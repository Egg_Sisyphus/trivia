#include "SqlliteDataBase.h"

DatabaseReturnData databaseReturnData;

int callback(void* data, int argc, char** argv, char** azColName)
{
    databaseReturnData.clear();
    for (int i = 0; i < argc; i++)
    {
        databaseReturnData.push_back(std::pair<std::string, std::string>(std::string(azColName[i]), std::string(argv[i])));
    }
    return 0;
}

DatabaseReturnData SqlliteDataBase::_runSqlCommand(std::string command)
{
    const char* sqlStatement = command.c_str();

    char* errMessage = nullptr;

    int res = sqlite3_exec(this->_database, sqlStatement, callback, nullptr, &errMessage);
    if (res != SQLITE_OK)
    {
        throw MyException("Invalid sqllite message: " + std::string(errMessage));
    }
    return databaseReturnData;
}

SqlliteDataBase::SqlliteDataBase(const std::string dataBaseFile)
{
    bool file_exist = _access(dataBaseFile.c_str(), 0) == 0;
    int res = sqlite3_open(dataBaseFile.c_str(), &this->_database);
    if (res != SQLITE_OK)
    {
        this->_database = nullptr;
        throw MyException("Database failed to open.");
    }

    if (!file_exist)
    {
        std::cout << "creating file" << std::endl;
        this->_runSqlCommand("CREATE TABLE USERS("
            "NAME TEXT PRIMARY KEY NOT NULL, "
            "PASSWORD TEXT NOT NULL, "
            "EMAIL TEXT NOT NULL"
            ");");
    }

}

SqlliteDataBase::~SqlliteDataBase()
{
    sqlite3_close(this->_database);
    this->_database = nullptr;
}

bool SqlliteDataBase::doesUserExist(std::string name)
{
    DatabaseReturnData got = this->_runSqlCommand("SELECT * FROM USERS WHERE NAME = \"" + name + "\";");
    if (!got.size())
        return false;
    return true;
}

bool SqlliteDataBase::doesPasswordMatch(std::string name, std::string password)
{
    DatabaseReturnData got = this->_runSqlCommand("SELECT * FROM USERS WHERE NAME = \"" + name + "\";");
    if (!got.size())
        return false;
    if (got[1].second != password)
        return false;
    return true;
}

void SqlliteDataBase::addNewUser(std::string name, std::string password, std::string email)
{
    DatabaseReturnData got = this->_runSqlCommand("INSERT INTO USERS VALUES(\"" + name + "\", \"" + password + "\", \"" + email + "\");");
    for (auto i : got)
    {
        std::cout << i.first << ", " << i.second << std::endl;
    }
    std::cout << std::endl << std::endl;

}
