#pragma once
#include "definitions.h"

class MyException : public std::exception
{
public:
    MyException(const std::string& issue) : m_message(issue) {}
    virtual const char* what() const noexcept { return this->m_message.c_str(); };

private:
    std::string m_message;
};