#pragma once
#include "definitions.h"

typedef struct LoginRequest
{
    std::string username;
    std::string password;
} LoginRequest;

typedef struct SignUpRequest
{
    std::string username;
    std::string password;
    std::string email;
} SignUpRequest;

class JsonRequestPacketDeserializer
{
public:
    static LoginRequest deserializeLoginRequest(Buffer buffer);
    static SignUpRequest deserializeSignUpRequest(Buffer buffer);

};

inline LoginRequest JsonRequestPacketDeserializer::deserializeLoginRequest(Buffer buffer)
{
    LoginRequest request = LoginRequest();
    try
    {
        json egg = json::parse(buffer.data());
        request.password = egg["password"];
        request.username = egg["username"];
    }
    catch (json::exception& e)
    {
        std::cout << "json error with text: " << BUFFER_TO_NICE(buffer) << std::endl;
        throw e;
    }
    return request;
}

inline SignUpRequest JsonRequestPacketDeserializer::deserializeSignUpRequest(Buffer buffer)
{
    SignUpRequest request = SignUpRequest();
    try
    {
        json egg = json::parse(buffer.data());
        request.password = egg["password"];
        request.username = egg["username"];
        request.email = egg["email"];
    }
    catch (json::exception& e)
    {
        std::cout << "json error with text: " << BUFFER_TO_NICE(buffer) << std::endl;
        throw e;
    }
    return request;
}
