#pragma once
#include "definitions.h"
#include "IDatabase.h"

struct LoggedUser
{
    std::string username;
};

class LoginManager
{
private:
    IDatabase* _Database;
    std::vector<LoggedUser> _LoggedUsers;
    bool _isConnected(std::string name);

public:
    LoginManager(IDatabase* dataBase);
    bool login(std::string name, std::string password);
    bool signUp(std::string name, std::string password, std::string email);
    bool logout(std::string name);

protected:

};

inline LoginManager::LoginManager(IDatabase* dataBase)
{
    this->_Database = dataBase;
}

inline bool LoginManager::_isConnected(std::string name)
{
    return std::any_of(this->_LoggedUsers.begin(), this->_LoggedUsers.end(), [name](LoggedUser user) {return user.username == name; });
}

inline bool LoginManager::login(std::string name, std::string password)
{
    if (this->_isConnected(name))
        return false;
    if (!this->_Database->doesPasswordMatch(name, password))
        return false;
    this->_LoggedUsers.push_back(LoggedUser{ name });
    return true;
}

inline bool LoginManager::signUp(std::string name, std::string password, std::string email)
{
    if (this->_Database->doesUserExist(name))
        return false;
    this->_Database->addNewUser(name, password, email);
    this->_LoggedUsers.push_back(LoggedUser{ name });
    return true;
}

inline bool LoginManager::logout(std::string name)
{
    auto target = std::find_if(this->_LoggedUsers.begin(), this->_LoggedUsers.end(), [name](LoggedUser user) {return user.username == name; });
    if (target == this->_LoggedUsers.end())
        return false;
    this->_LoggedUsers.erase(target);
    return true;
}

