#pragma once
#include "definitions.h"

class IRequestHandler;


struct RequestResult
{
    Buffer response;
    IRequestHandler* newHandler;
};
struct RequestInfo
{
    int id;
    std::time_t receivalTime;
    Buffer buffer;

};

class IRequestHandler
{
public:
    virtual bool isRequestRelevant(RequestInfo) = 0;
    virtual RequestResult handleRequest(RequestInfo) = 0;

private:


protected:
    SOCKET _socket = NULL;
};