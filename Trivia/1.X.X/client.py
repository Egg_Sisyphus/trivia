import socket
import json

def get_input(*args):
	data = {}
	for item in args:
		data[item] = input(f"{item}: ")
	return data

try:
	with socket.socket() as sock:
		sock.connect(("127.0.0.1", 8876));
		data = {}
		choice = input("which option?")
		while choice != "-1":
			if choice == '0':
				data = get_input("username", "password")
				code = b"\x00"
			elif choice == '1':
				data = get_input("username", "password", "email")
				code = b"\x01"
			else:
				choice = input("which option?")
				continue
				

			ans = json.dumps(data)
			sock.send(code + len(ans).to_bytes(4, "little") + ans.encode());
			got = sock.recv(1024)
			if not got:
				print("client disconneced from the server")
				break
			print("got:", got)
			choice = input("which option?")
except Exception as e:
	print(e)
	input()
