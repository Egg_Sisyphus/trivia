#pragma once
#include <WinSock2.h>
#include <Windows.h>
#include <exception>
#pragma comment (lib, "ws2_32.lib")


class WSAInitializer
{
public:
    WSAInitializer();
    ~WSAInitializer();
};

inline WSAInitializer::WSAInitializer()
{
    WSADATA wsa_data = { };
    if (WSAStartup(MAKEWORD(2, 2), &wsa_data) != 0)
        throw std::exception("WSAStartup Failed");
}

inline WSAInitializer::~WSAInitializer()
{
    try
    {
        WSACleanup();
    }
    catch (...) {}
}
