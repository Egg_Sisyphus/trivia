#pragma once
#include "definitions.h"

typedef struct LoginResponse
{
    unsigned int status;
} LoginResponse;

typedef struct SignUpResponse
{
    unsigned int status;
} SignUpResponse;

typedef struct ErrorResponse
{
    std::string message;
} ErrorResponse;

class JsonResponsePacketSerializer
{
public:
    static Buffer serializeLoginResponse(LoginResponse response);
    static Buffer serializeSignUpResponse(SignUpResponse response);
    static Buffer serializeErrorResponse(ErrorResponse response);

private:
    static Buffer SerializeFormatResponse(byte code, std::string jsonString);
};

inline Buffer JsonResponsePacketSerializer::serializeLoginResponse(LoginResponse response)
{
    json egg;
    egg["status"] = response.status;
    std::string got = egg.dump();
    return JsonResponsePacketSerializer::SerializeFormatResponse(1, got);
}

inline Buffer JsonResponsePacketSerializer::serializeSignUpResponse(SignUpResponse response)
{
    json egg;
    egg["status"] = response.status;
    std::string got = egg.dump();
    return JsonResponsePacketSerializer::SerializeFormatResponse(1, got);
}

inline Buffer JsonResponsePacketSerializer::serializeErrorResponse(ErrorResponse response)
{
    json egg;
    egg["message"] = response.message;
    std::string got = egg.dump();
    return JsonResponsePacketSerializer::SerializeFormatResponse(1, got);
}

/*
formats the response to be in the <code><length><message> format
*/
inline Buffer JsonResponsePacketSerializer::SerializeFormatResponse(byte code, std::string jsonString)
{
    Buffer buff = Buffer(jsonString.size() + 5);
    buff[0] = code;
    buff[1] = jsonString.size();
    memcpy(&buff[5], jsonString.data(), jsonString.size());

    return buff;
}

