#include "LoginRequestHandler.h"

LoginRequestHandler::LoginRequestHandler(LoginManager& manager, RequestHandlerFactory& handlerFactory) : _handlerFactory(handlerFactory), _loginManager(manager)
{
}

bool LoginRequestHandler::isRequestRelevant(RequestInfo info)
{
    return (info.id == LOGIN_REQUEST_ID or info.id == SIGNUP_REQUEST_ID);
    // json egg = json::parse(info.buffer);
    // return egg.contains("username") and egg.contains("password");
}

RequestResult LoginRequestHandler::handleRequest(RequestInfo info)
{
    if (info.id == LOGIN_REQUEST_ID)
    {
        return this->login(info);
    }
    else
    {
        return this->signUp(info);
    }
}

RequestResult LoginRequestHandler::login(RequestInfo info)
{
    RequestResult res;
    //res.newHandler = new LoginRequestHandler();
    LoginRequest request = JsonRequestPacketDeserializer::deserializeLoginRequest(info.buffer);
    LoginResponse response = { 0 };
    response.status = this->_loginManager.login(request.username, request.password);
    res.newHandler = this->_handlerFactory.createLoginRequestHandler();
    Buffer buff = JsonResponsePacketSerializer::serializeLoginResponse(response);
    res.response = buff;
    return res;
}

RequestResult LoginRequestHandler::signUp(RequestInfo info)
{
    RequestResult res;
    res.newHandler = nullptr;
    SignUpRequest request = JsonRequestPacketDeserializer::deserializeSignUpRequest(info.buffer);
    SignUpResponse response = { 0 };

    response.status = this->_loginManager.signUp(request.username, request.password, request.email);
    Buffer buff = JsonResponsePacketSerializer::serializeSignUpResponse(response);
    res.response = buff;
    return res;
}
