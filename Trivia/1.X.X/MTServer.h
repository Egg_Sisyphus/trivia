#pragma once
#include "definitions.h"
#include "Communicator.h"
#include "IDatabase.h"
#include "RequestFactory.h"
#include "SqlliteDataBase.h"


class MTServer
{
public:
    MTServer(int server_port);
    ~MTServer();
    void run();

private:
    IDatabase* _database;
    RequestHandlerFactory _handleFactory;
    Communicator _communicator;
    void serverCommands();
    std::vector<std::thread> _threads;

};
