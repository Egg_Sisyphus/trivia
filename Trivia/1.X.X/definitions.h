#pragma once
#include <WinSock2.h>
#include <Windows.h>
#include <string>
#include <iostream>
#include <thread>
#include <map>
#include <vector>
#include <nlohmann/json.hpp>
#include <utility>
#include <ctime>
#include "MyException.hpp"
#include <io.h>
#include <mutex>

enum RequestId
{
    LOGIN_REQUEST_ID,
    SIGNUP_REQUEST_ID,
};

using json = nlohmann::json;

typedef std::vector<byte> Buffer;
#define BUFFER_TO_NICE(vec) (char*)vec.data()

