#pragma once
#include "definitions.h"
#include "LoginManager.h"
#include "LoginRequestHandler.h"

class LoginRequestHandler;

class RequestHandlerFactory
{
private:
    IDatabase* _database;
    LoginManager _loginManager;
    //    MenuManager _menuManager;
public:
    RequestHandlerFactory(IDatabase* dataBase);
    LoginRequestHandler* createLoginRequestHandler();
    LoginManager& getLoginManager();
protected:
};
