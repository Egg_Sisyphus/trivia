#pragma once
#include "IRequestHandler.h"
#include "WSAInitializer.hpp"
#include "LoginRequestHandler.h"
#include <map>

class Communicator
{
public:
    Communicator(RequestHandlerFactory& handlerFactory);
    ~Communicator();
    void startHandleRequests();

private:
    std::map<SOCKET, IRequestHandler*> _clients;
    RequestHandlerFactory& _handlerFactory;
    SOCKET _serverSocket;

    int _server_port;
    WSAInitializer _wsaInit;

    void bindAndListen();
    void handleNewClient(SOCKET clientSocket);
};

