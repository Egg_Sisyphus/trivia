#pragma once
#include "LoginManager.h"
#include "RequestFactory.h"
#include "IRequestHandler.h"
#include "JsonRequestPacketDeserializer.h"
#include "JsonResponsePacketSerializer.h"

class RequestHandlerFactory;

class LoginRequestHandler : public IRequestHandler
{
public:
    LoginRequestHandler(LoginManager& manager, RequestHandlerFactory& handlerFactory);
    virtual bool isRequestRelevant(RequestInfo) override;
    virtual RequestResult handleRequest(RequestInfo) override;

private:
    LoginManager& _loginManager;
    RequestHandlerFactory& _handlerFactory;
    RequestResult login(RequestInfo);
    RequestResult signUp(RequestInfo);
};

