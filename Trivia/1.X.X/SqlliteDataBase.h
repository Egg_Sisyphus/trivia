#pragma once
#include "definitions.h"
#include "IDatabase.h"
#include "sqlite3.h"

typedef std::vector<std::pair<std::string, std::string>> DatabaseReturnData;

class SqlliteDataBase : public IDatabase
{
private:
    sqlite3* _database;
    DatabaseReturnData _runSqlCommand(std::string command);

public:
    SqlliteDataBase(const std::string dataBaseFile);
    ~SqlliteDataBase();
    // Inherited via IDatabase
    virtual bool doesUserExist(std::string name) override;
    virtual bool doesPasswordMatch(std::string name, std::string password) override;
    virtual void addNewUser(std::string name, std::string password, std::string email) override;
};

