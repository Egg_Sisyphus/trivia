#include "MTServer.h"

#define EXIT_COMMAND "EXIT"

MTServer::MTServer(int server_port) : _database(new SqlliteDataBase("Magshimim.db")), _handleFactory(this->_database), _communicator(this->_handleFactory)
{
    this->_threads.push_back(std::thread(&MTServer::serverCommands, this));
}



MTServer::~MTServer()
{

}

void MTServer::run()
{
    try
    {
        this->_communicator.startHandleRequests();
    }
    catch (std::exception& e)
    {
        std::cout << "Error occured: " << e.what() << std::endl;
    }
}

void MTServer::serverCommands()
{
    std::string input;
    while (true)
    {
        std::cin >> input;
        if (input == EXIT_COMMAND)
        {
            std::cout << "egg";
        }
        else
        {
            std::cout << "COMMAND NOT FOUND" << std::endl;
        }

    }
}
