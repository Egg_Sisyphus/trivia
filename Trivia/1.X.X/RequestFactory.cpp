#include "RequestFactory.h"

RequestHandlerFactory::RequestHandlerFactory(IDatabase* dataBase) : _database(dataBase), _loginManager(this->_database)
{
}

LoginRequestHandler* RequestHandlerFactory::createLoginRequestHandler()
{
    return new LoginRequestHandler(this->_loginManager, *this);
}

LoginManager& RequestHandlerFactory::getLoginManager()
{
    return this->_loginManager;
}
