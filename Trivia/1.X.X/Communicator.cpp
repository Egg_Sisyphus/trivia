#include "Communicator.h"
#include <iostream>
#include <thread>

Communicator::Communicator(RequestHandlerFactory& handlerFactory) : _handlerFactory(handlerFactory)
{
    this->_wsaInit = WSAInitializer();

    this->_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

    if (this->_serverSocket == INVALID_SOCKET)
        throw std::exception(__FUNCTION__ " - socket");

    this->_server_port = 8876;
}

Communicator::~Communicator()
{
    for (std::pair<const SOCKET, IRequestHandler*> clientPair : this->_clients)
    {
        try { closesocket(clientPair.first); }
        catch (...) {}
        delete clientPair.second;
    }
    try
    {
        // the only use of the destructor should be for freeing 
        // resources that was allocated in the constructor
        closesocket(this->_serverSocket);
    }
    catch (...) {}
}

void Communicator::startHandleRequests()
{
    this->bindAndListen();
    std::vector<std::thread> threads = std::vector<std::thread>();
    while (true)
    {
        // the main thread is only accepting clients 
        // and add then to the list of handlers
        std::cout << "Waiting for client connection request" << std::endl;
        SOCKET clientSocket = accept(this->_serverSocket, NULL, NULL);
        if (clientSocket == INVALID_SOCKET)
            throw std::exception(__FUNCTION__);

        std::cout << "Client accepted. Server and client can speak" << std::endl;
        this->_clients[clientSocket] = this->_handlerFactory.createLoginRequestHandler();
        // the function that handle the conversation with the client
        threads.push_back(std::thread(&Communicator::handleNewClient, this, clientSocket));
    }
}

void Communicator::bindAndListen()
{
    struct sockaddr_in sa = { 0 };

    sa.sin_port = htons(this->_server_port); // port that server will listen for
    sa.sin_family = AF_INET;   // must be AF_INET
    sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

    // Connects between the socket and the configuration (port and etc..)
    if (bind(_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
        throw std::exception(__FUNCTION__ " - bind");

    // Start listening for incoming requests of clients
    if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
        throw std::exception(__FUNCTION__ " - listen");
    std::cout << "Listening on port " << this->_server_port << std::endl;
}

void Communicator::handleNewClient(SOCKET clientSocket)
{
    try
    {
        IRequestHandler* handler = this->_clients[clientSocket];
        while (handler)
        {
            char info[6] = { 0 };
            if (recv(clientSocket, info, 5, 0) == -1)
                break;
            RequestInfo requestInfo = {};
            requestInfo.id = (RequestId)info[0];
            int messageLength = 0;
            memcpy(&messageLength, info + 1, 4);
            requestInfo.buffer = Buffer(messageLength + 1);
            if (recv(clientSocket, (char*)requestInfo.buffer.data(), messageLength, 0) == -1)
                break;
            if (!handler->isRequestRelevant(requestInfo))
                continue;
            RequestResult response = handler->handleRequest(requestInfo);
            delete handler;
            this->_clients[clientSocket] = response.newHandler;
            send(clientSocket, (char*)response.response.data(), response.response.size(), 0);
            handler = this->_clients[clientSocket];
        }
    }
    catch (std::exception& e)
    {
        closesocket(clientSocket);
        std::cout << "CLIENT KICKED " << e.what() << std::endl;
        throw e;
    }
    std::cout << "client exited gracefully\n";
    closesocket(clientSocket);

}
